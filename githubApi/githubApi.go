package githubApi

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"bytes"
	"log"
)

const (
	credfilename string = "githubapicred.json"
	baseApi string = "https://api.github.com/"
)

func Auth(args []string) {
	name := args[0]
	token := args[1]

	credentials := &Credentials{name, token}
	b, err := json.Marshal(credentials)
	if err != nil {
		fmt.Println(err)
		return
	}

	ioutil.WriteFile(getFilePath(), b, 0644)
	// TODO: check if credentials are okay (response status)
}

func CreateRepo(args []string) {
	newRepoName := args[0]
	credentials, err := getCredentials()
	if err != nil {
		log.Fatal(err)
	}

	newRepo := &Repo{newRepoName}
	b, err := json.Marshal(newRepo)
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("%suser/repos", baseApi)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "token " + credentials.Token)

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	if res.StatusCode == 201 {
		fmt.Printf("Repo %s succesfully created\n", newRepoName)
	} else {
		fmt.Println("Repo wasn't created. =(")
	}
}

func DeleteRepo(args []string) {
	repoName := args[0]
	credentials, err := getCredentials()
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("%srepos/%s/%s", baseApi, credentials.Name, repoName)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", "token " + credentials.Token)

	client := http.Client{}
	_, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Repo %s probably was succesfully deleted\n", repoName)
}

func CreateIssue(args []string) {
	repoName := args[0]
	titleIssue := args[1]
	textIssue := args[2]

	credentials, err := getCredentials()
	if err != nil {
		log.Fatal(err)
	}

	newIssue := &Issue{titleIssue, textIssue}
	b, err := json.Marshal(newIssue)
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("%srepos/%s/%s/issues", baseApi, credentials.Name, repoName)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(b))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "token " + credentials.Token)

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	var createIssueResponse CreateIssueResponse
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(body, &createIssueResponse)

	if res.StatusCode == 201 {
		fmt.Printf("Issue #%d on %s succesfully created\n", createIssueResponse.Number, repoName)
	} else {
		fmt.Println("Issue wasn't created. =(")
	}
}

func Comments(args []string) {
	repoName := args[0]
	issueId := args[1]
	credentials, err := getCredentials()
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("%srepos/%s/%s/issues/%s/comments", baseApi, credentials.Name, repoName, issueId)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", "token " + credentials.Token)

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	comments := make([]Comment, 0)
	json.Unmarshal(body, &comments)
	for _, comment := range comments {
		fComment := fmt.Sprintf("%s said: %s", comment.User.Login, comment.Body)
		fmt.Println(fComment)
	}
}
