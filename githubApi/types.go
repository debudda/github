package githubApi

type Credentials struct {
	Name, Token string
}

type Repo struct {
	Name string `json:"name"`
}

type Issue struct {
	Title string `json:"title"`
	Body string `json:"body"`
}

type CreateIssueResponse struct {
	Number int `json:"number"`
}

type User struct {
	Login string `json:"login"`
}

type Comment struct {
	Body string `json:"body"`
	User User `json:"user"`
}