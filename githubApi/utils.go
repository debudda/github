package githubApi

import (
	"path"
	"io/ioutil"
	"encoding/json"
	"os/user"
	"log"
	"net/http"
)

func getFilePath() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return path.Join(usr.HomeDir, credfilename)
}

func getCredentials() (*Credentials, error) {
	var credentials Credentials
	jsonCredentials, err := ioutil.ReadFile(getFilePath())
	if err != nil {
		return nil, err
	}
	json.Unmarshal(jsonCredentials, &credentials)
	return &credentials, nil
}

func addAuthHeader(req http.Request, token string) {
	req.Header.Set("Authorization", "token " + token)
}