package main

import (
	"bitbucket.org/github/argsRouter"
	"os"
)

func main() {
	if (len(os.Args) > 1) {
		argsRouter.Read(os.Args[1])
	} else {
		argsRouter.Help("all")
	}
}
