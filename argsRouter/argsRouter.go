package argsRouter

import (
	"fmt"
	"os"
	"bufio"
	"bitbucket.org/github/githubApi"
	"strings"
)

var cmds map[string]string = map[string]string{
	"auth": "<username> <token>",
	"create-repo": "<repoName>",
	"delete-repo": "<repoName>",
	"create-issue": "<repoName> <title> <text>",
	"comments": "<repoName> <issueId>",
}

func Help(cmd string) {
	if cmd == "all" {
		fmt.Println("Available commands: ")
		for key, value := range cmds {
			helpLine := fmt.Sprintf("%s: %s", key, value)
			fmt.Println(helpLine)
		}
	} else {
		helpLine := fmt.Sprintf("%s: %s", cmd, cmds[cmd])
		fmt.Println(helpLine)
	}
}

// Recursively control required args provision
func ensureProvided(arguments, options []string) []string {
	argsLen := len(arguments)
	optionsLen := len(options)
	if argsLen < optionsLen {
		Help(os.Args[1])
		reader := bufio.NewReader(os.Stdin)
		fmt.Printf("Please, provide %s: ", options[argsLen])
		newArgValue, _ := reader.ReadString('\n')
		return ensureProvided(
			append(
				arguments,
				strings.TrimSuffix(newArgValue, "\n"),
			),
			options,
		)
	}
	return arguments
}

func Read(cmd string) {
	var args []string
	providedOptions := os.Args[2:] // get rid of package name and cmd

	switch cmd {
	case "auth":
		args = ensureProvided(providedOptions, []string{"username", "token"})
		githubApi.Auth(args)
	case "create-repo":
		args = ensureProvided(providedOptions, []string{"repo"})
		githubApi.CreateRepo(args)
	case "delete-repo":
		args = ensureProvided(providedOptions, []string{"repo"})
		githubApi.DeleteRepo(args)
	case "create-issue":
		args = ensureProvided(providedOptions, []string{"repo", "title", "text"})
		githubApi.CreateIssue(args)
	case "comments":
		args = ensureProvided(providedOptions, []string{"repo", "issue id"})
		githubApi.Comments(args)
	default:
		fmt.Println("I can't find such a command.")
		Help("all")
	}
}